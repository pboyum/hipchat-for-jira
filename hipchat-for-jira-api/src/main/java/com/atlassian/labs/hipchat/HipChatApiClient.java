package com.atlassian.labs.hipchat;

import com.atlassian.sal.api.net.ResponseException;

import java.util.Collection;

/**
 * This interface (including #BackgroundColour) is API. Please make only additive changes to it until the next X.0 release
 */
public interface HipChatApiClient
{
    boolean isAuthTokenValid(String token) throws ResponseException;

    Collection<Room> getRooms() throws ResponseException;

    void notifyRoom(String room, String msg, boolean notifyClients) throws ResponseException;

    void notifyRoom(String room, String msg, boolean notifyClients, BackgroundColour colour) throws ResponseException;

    public static enum BackgroundColour {
        YELLOW("yellow"),
        RED("red"),
        GREEN("green"),
        PURPLE("purple");

        public String value;

        BackgroundColour(String value) {
            this.value = value;
        }
    }

}
